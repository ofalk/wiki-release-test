# wiki-release-test

Simple and pretty straight forward project to showcase how you can add your project wiki as a submodule (see `.gitmodules`) and pull the latest content from the same during a pipeline run (see `.gitlab-ci.yml`).

Not that during the CI run, the updated submodules aren't pushed back to the main repo - this is mainly intended to ensure only the latest wiki content is used during the run, but could also use the commited one of course (just remove the `--remote` flag from the `submodule update` command in the `.gitlab-ci.yml`).

Note that you don't necessarily need to have the wiki as a submodule in your code if you want to always use the latest and greatest version! You could also make this only part of your CI config.

## License
Free to use - no copyright at all.